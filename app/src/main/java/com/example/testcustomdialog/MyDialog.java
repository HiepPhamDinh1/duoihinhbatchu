package com.example.testcustomdialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;

import androidx.annotation.NonNull;

public class MyDialog extends Dialog {
    private MyDialog myDialog;
    private Button btCancel , btOk;
    public MyDialog(@NonNull Context context) {
        super(context);
        myDialog = new MyDialog(context);
        myDialog.setContentView(R.layout.bg_dialog);

    }

    private void initEvents() {
        btCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }

    private void initViews() {
        btCancel = myDialog.findViewById(R.id.bt_cancel);
        btOk = myDialog.findViewById(R.id.bt_ok);
    }
}
